evgenConfig.generators  += ["Powheg", "Pythia8"]
evgenConfig.description  = 'Powheg WWbb production, including interference between ttbar and Wt.  A14 NNPDF23 tune'
evgenConfig.keywords    += [ 'SM', 'top', 'WW', 'lepton']
evgenConfig.contact      = [ 'Ben Nachman <bnachman@cern.ch>' 'James Monk <jmonk@cern.ch>']

# --------------------------------------------------------------
# Load ATLAS defaults for the Powheg bblvlv process
# --------------------------------------------------------------
include("PowhegControl/PowhegControl_bblvlv_Common.py")

# --------------------------------------------------------------
# Generate events
# --------------------------------------------------------------
PowhegConfig.nEvents=130000
PowhegConfig.allrad=1
PowhegConfig.ncall1=5000000
#PowhegConfig.manyseeds=1
#PowhegConfig.use-old-grid=1

PowhegConfig.generate()


include('MC15JobOptions/Pythia8_A14_NNPDF23LO_EvtGen_Common.py')
include('MC15JobOptions/Pythia8_Powheg_Main31.py')

genSeq.Pythia8.UserHooks += ['PowhegBB4L']

genSeq.Pythia8.Commands += [  "Powheg:pTHard = 0",
                              "Powheg:NFinal = 2",
                              "Powheg:bb4l:onlyDistance1 = 1"]

