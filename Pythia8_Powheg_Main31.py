## Configure Pythia 8 to shower PoWHEG input using Main31 shower veto
include("MC15JobOptions/Pythia8_Powheg.py")
genSeq.Pythia8.Commands += [ 'SpaceShower:pTmaxMatch = 2',
                             'TimeShower:pTmaxMatch = 2'  ]

if "UserHooks" in genSeq.Pythia8.__slots__.keys():
  genSeq.Pythia8.UserHooks += ['PowhegMain31']
  genSeq.Pythia8.Commands += ['Powheg:veto = 1']
else:
  genSeq.Pythia8.UserHook = 'Main31'


#
# Pythia8 shower weights are only available in release 2.26 and later.  The
# test below checks the Pythia8 release and also verifies that the Pythia_i
# tag is recent enought to store the shower weights.
ver =  os.popen("cmt show versions External/Pythia8").read()
parsed = ver.split(" ")
pieces = parsed[1].split("-")
print "Pythia Version 8.",pieces[2]
if int(pieces[2])>=26:
   if "ShowerWeightNames" in genSeq.Pythia8.__slots__.keys():
      print "Initalizing Shower Weights in Pythia8_Powheg_Main31.py"
      genSeq.Pythia8.Commands += [
                            'UncertaintyBands:doVariations = on', 
                            'UncertaintyBands:List = {\
                             Var3cUp isr:muRfac=0.549241,\
                             Var3Down isr:muRfac=1.960832,\
                             isr:muRfac=2.0_fsr:muRfac=2.0 isr:muRfac=2.0 fsr:muRfac=2.0,\
                             isr:muRfac=2.0_fsr:muRfac=1.0 isr:muRfac=2.0 fsr:muRfac=1.0,\
                             isr:muRfac=2.0_fsr:muRfac=0.5 isr:muRfac=2.0 fsr:muRfac=0.5,\
                             isr:muRfac=1.0_fsr:muRfac=2.0 isr:muRfac=1.0 fsr:muRfac=2.0,\
                             isr:muRfac=1.0_fsr:muRfac=0.5 isr:muRfac=1.0 fsr:muRfac=0.5,\
                             isr:muRfac=0.5_fsr:muRfac=2.0 isr:muRfac=0.5 fsr:muRfac=2.0,\
                             isr:muRfac=0.5_fsr:muRfac=1.0 isr:muRfac=0.5 fsr:muRfac=1.0,\
                             isr:muRfac=0.5_fsr:muRfac=0.5 isr:muRfac=0.5 fsr:muRfac=0.5,\
                             isr:muRfac=1.75_fsr:muRfac=1.0 isr:muRfac=1.75 fsr:muRfac=1.0,\
                             isr:muRfac=1.5_fsr:muRfac=1.0 isr:muRfac=1.5 fsr:muRfac=1.0,\
                             isr:muRfac=1.25_fsr:muRfac=1.0 isr:muRfac=1.25 fsr:muRfac=1.0,\
                             isr:muRfac=0.625_fsr:muRfac=1.0 isr:muRfac=0.625 fsr:muRfac=1.0,\
                             isr:muRfac=0.75_fsr:muRfac=1.0 isr:muRfac=0.75 fsr:muRfac=1.0,\
                             isr:muRfac=0.875_fsr:muRfac=1.0 isr:muRfac=0.875 fsr:muRfac=1.0,\
                             isr:muRfac=1.0_fsr:muRfac=1.75 isr:muRfac=1.0 fsr:muRfac=1.75,\
                             isr:muRfac=1.0_fsr:muRfac=1.5 isr:muRfac=1.0 fsr:muRfac=1.5,\
                             isr:muRfac=1.0_fsr:muRfac=1.25 isr:muRfac=1.0 fsr:muRfac=1.25,\
                             isr:muRfac=1.0_fsr:muRfac=0.625 isr:muRfac=1.0 fsr:muRfac=0.625,\
                             isr:muRfac=1.0_fsr:muRfac=0.75 isr:muRfac=1.0 fsr:muRfac=0.75,\
                             isr:muRfac=1.0_fsr:muRfac=0.875 isr:muRfac=1.0 fsr:muRfac=0.875\
                             }'] 

      genSeq.Pythia8.ShowerWeightNames = [ 
                             "Var3cUp",
                             "Var3Down",
                             "isr:muRfac=2.0_fsr:muRfac=2.0",
                             "isr:muRfac=2.0_fsr:muRfac=1.0",
                             "isr:muRfac=2.0_fsr:muRfac=0.5",
                             "isr:muRfac=1.0_fsr:muRfac=2.0",
                             "isr:muRfac=1.0_fsr:muRfac=0.5",
                             "isr:muRfac=0.5_fsr:muRfac=2.0",
                             "isr:muRfac=0.5_fsr:muRfac=1.0",
                             "isr:muRfac=0.5_fsr:muRfac=0.5",
                             "isr:muRfac=1.75_fsr:muRfac=1.0",
                             "isr:muRfac=1.5_fsr:muRfac=1.0",
                             "isr:muRfac=1.25_fsr:muRfac=1.0",
                             "isr:muRfac=0.625_fsr:muRfac=1.0",
                             "isr:muRfac=0.75_fsr:muRfac=1.0",
                             "isr:muRfac=0.875_fsr:muRfac=1.0",
                             "isr:muRfac=1.0_fsr:muRfac=1.75",
                             "isr:muRfac=1.0_fsr:muRfac=1.5",
                             "isr:muRfac=1.0_fsr:muRfac=1.25",
                             "isr:muRfac=1.0_fsr:muRfac=0.625",
                             "isr:muRfac=1.0_fsr:muRfac=0.75",
                             "isr:muRfac=1.0_fsr:muRfac=0.875"
                             ]
   else:
      print "Pythia8_i version too old to support isr/fsr shower weights"

else:
   print "No shower weights since not supported in Pythia8.2",pieces[2]


